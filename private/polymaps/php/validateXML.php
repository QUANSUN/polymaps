﻿<?php

if (isset($_POST['xmlstring'])) {

	$dom = new DOMDocument();
	$dom->loadXML(str_replace('xmlns="http://www.w3.org/1999/xhtml"', '',$_POST['xmlstring']));
	$dom->encoding = 'utf-8';

	$schema = "../res/campus.xsd";


	// Validation du document XML
	$validate = $dom->schemaValidate($schema) ?
	"validate" :
	"fail";

	if ($validate === "validate") {
		// écraser l'ancien XML
		$dom->save("../res/dialecte.xml");
	}
	echo $validate;

} else {
	echo 'give me a xml string please';
}

?>