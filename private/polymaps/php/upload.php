<?php
if(isset($_FILES["file"]["type"]))
{
     $validextensions = array("png");
     $temporary = explode(".", $_FILES["file"]["name"]);
     $file_extension = end($temporary);
     if (((($_FILES["file"]["type"] == "image/png") ))//Approx. 100kb files can be uploaded.
     && in_array($file_extension, $validextensions)) {
          if ($_FILES["file"]["error"] > 0)
          {
               echo "Return Code: " . $_FILES["file"]["error"] . "<br/><br/>";
          }
          else
          {
               if (file_exists("../images/menubar/" . $_FILES["file"]["name"])) {
                    echo "already exists";
               }
               else
               {
                    $sourcePath = $_FILES['file']['tmp_name']; // Storing source path of the file in a variable
                    $targetPath = "../images/menubar/".str_replace(" ", "", $_POST["typeIcon"]).".png"; // Target path where file is to be stored
                    move_uploaded_file($sourcePath,$targetPath) ; // Moving Uploaded file
                    echo "success";
               }
          }
     }
     else
     {
          echo "<span id='invalid'>***Invalid file  Type***<span>";
     }
}
?>