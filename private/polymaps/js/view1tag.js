﻿function suggest(tagname) {
    if (tagname != '') {
        document.querySelector('#results').innerHTML = '';
        var tabtag = searchTag(tagname);

        for (var i = 0; i < tabtag.length; i++) {
            var htmltoadd = '<div class="resultat" onclick="displaySalles(\''+tabtag[i]+'\')"><table class="table table-hover"><td class="tagname"><span class="brace">{</span>'+tabtag[i]+'<span class="brace">}</span></td>'+
            '<td><img src="./images/ionicon/arrow-right-c.png"/></td> </div>';
            document.querySelector('#results').innerHTML += htmltoadd;
        }

        recurseSuggest(1);
    } else {
        document.querySelector('#results').innerHTML = '';
    }

    markers.clearLayers();
    map.removeLayer(markers);

    
}

function recurseSuggest(num) {
    setTimeout(function() {
        try {
            document.querySelector('#results .resultat:nth-child('+num+')').style.opacity = '1';
            recurseSuggest(num+1);
        } catch (err) {}

    },50);
}

var markers = L.layerGroup();
function displaySalles(tagname) {
    var sallesArray = getSallesByTagName(tagname);
    if (mobile) {
        for (var i = 0; i < sallesArray.length; i++) {
            sallesArray[i][4] = (i == 0);
        }
        window.localStorage.setItem('listsalles',JSON.stringify(sallesArray));
        // go to the next page
        window.location.href = './View3.html';
    } else {
        document.querySelector('#results').innerHTML = '';
        document.querySelector('#search input').value = '';
        
        /** TODO placer les pins et clean les suggestions **/
        document.querySelector('#results').innerHTML = '';
        for (var i = 0; i < sallesArray.length; i++) {
            var marker = L.marker(sallesArray[i][3]);
            marker.bindPopup('Batiment : ' + sallesArray[i][0]
                + '<br/>Etage : ' + sallesArray[i][1]
                + '<br/>Salle : ' + sallesArray[i][2]
                + '<br/><br/> <button class="btn btn-primary" style="width:100%" onclick="goTo(\''+ sallesArray[i][0] + '\',\'' + sallesArray[i][1]
                + '\',\'' + sallesArray[i][3]
                +'\')"> Go ! </button>'
            );
            markers.addLayer(marker);
        }
        markers.addTo(map);
    }
}

function goTo(batname, etagename, position) {
    window.localStorage.setItem('currentbat',batname);
    window.localStorage.setItem('currentetage',etagename);
    window.localStorage.setItem('position',position);
    document.location.href = './View3.html';
}