﻿function getXDomainRequest() {
	var xdr = null;
	if (window.XDomainRequest) { xdr = new XDomainRequest(); }
	else if (window.XMLHttpRequest) { xdr = new XMLHttpRequest(); }
	else { alert("Check your internet connexion !"); }
	return xdr;	
}

function getGeoJSON() {
	var xdr = getXDomainRequest();
     
	xdr.onload = function () {
		window.localStorage.setItem('fullXML',xdr.responseText);
		getXML();
	};
	xdr.open("POST", "./php/getJSON.php", true);
	xdr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	xdr.send();
}

var campusXML;
function getXML() {
	var xdr = getXDomainRequest();
     
	xdr.onload = function (reponse) {
		var xmlresponse = xdr.responseText;
		xmlresponse = xmlresponse.replace(/&lt;/g, "<").replace(/&gt;/g, ">")
									.replace(/&quot;/g,'"');
		
		if (window.DOMParser)
		{
			parser=new DOMParser();
			xmlDoc=parser.parseFromString(xmlresponse,"text/xml");
		}
		else // Internet Explorer
		{
			xmlDoc=new ActiveXObject("Microsoft.XMLDOM");
			xmlDoc.async=false;
			xmlDoc.loadXML(xmlresponse); 
		}
		campusXML = xmlDoc;
		initmap(getCampusPoint());
	};
	xdr.open("POST", "php/getXML.php", true);
	xdr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	xdr.send();
}

function getAllTab() {
	return ['Escaliers', 'Bâtiments'];
}

function getAll() {
	var result = '[';
	result += '[' + getCampusEscaliers() + ']';
	result += ','
	result += '[' + getBatiments() + ']';
	
	result += ']';

	return JSON.parse(result);
}

function getCampusPoint() {
	var points = JSON.parse(window.localStorage.getItem('fullXML')).point;
	var result = new Array();
	for (var i = 0; i < points.length; i++) {
		result.push([points[i]['@attributes'].y,points[i]['@attributes'].x])
	}
	return result;
}

function getCampusEscaliers() {
	var escTab = JSON.parse(window.localStorage.getItem('fullXML')).listescalier.escalier;

	var result = '';
	for (var i = 0; i < escTab.length; i++) {

		var points = escTab[i].point;

		
			result += '{ "type": "Feature",'
			+ '"properties": { "type" : "escalier" },'
			+ '"geometry": {'
				+ '"type": "LineString",'
				+ '"coordinates":['

		for (var j = 0; j < points.length; j++) {
			var coord = points[j]["@attributes"];
			result += '[' + coord.x + ','+ coord.y + ']';

			if (j < points.length -1) {
				result += ',';
			}
		}
		result += '] } }';
		if (i < escTab.length -1) {
			result += ',';
		}
	}
	//console.log(result);
	return result;
}

function getBatiments() {
	var batTab = JSON.parse(window.localStorage.getItem('fullXML')).listbatiment.batiment;
	
	var result = '';
	for (var i = 0; i < batTab.length; i++) {

		var points = batTab[i].point;
		
			result += '{ "type": "Feature",'
			+ '"properties": { "type" : "batiment", "name" : "' + batTab[i]['@attributes'].nom + '" },'
			+ '"geometry": {'
				+ '"type": "Polygon",'
				+ '"coordinates":[['

		for (var j = 0; j < points.length; j++) {
			var coord = points[j]["@attributes"];
			result += '[' + coord.x + ','+ coord.y + ']';

			if (j < points.length -1) {
				result += ',';
			}
		}
		result += ']] } }';
		if (i < batTab.length -1) {
			result += ',';
		}
	}
	//console.log(result);
	return result;
}

function getCampusEscaliersArray() {
	return JSON.parse('[' + getCampusEscaliers(JSON.parse(window.localStorage.getItem('fullXML')))+ ']');
}

function getCampusBatimentsArray() {
	return JSON.parse('[' + getBatiments(JSON.parse(window.localStorage.getItem('fullXML')))+ ']');
}

function getCampusElements() {
	var tabElement = new Array();

	try {
		if (JSON.parse(window.localStorage.getItem('fullXML')).listbatiment.batiment.length > 0) {
			var batiments = JSON.parse(window.localStorage.getItem('fullXML')).listbatiment.batiment;
			for (var i = 0; i < batiments.length; i++) {
				var batiment = batiments[i];
				traiterBatiment(batiment, tabElement, batiment['@attributes'].nom);
			}
		} else {
			var batiment = JSON.parse(window.localStorage.getItem('fullXML')).listbatiment.batiment;
			traiterBatiment(batiment, tabElement, batiment['@attributes'].nom);
		}
	} catch (err) {}


	return tabElement;
}

function getCampusElementsForBatiment() {
	var elements = getCampusElements();
	var positionToDelete = Array();
	for (var i = 0; i < elements.length; i++) {
		for (var j = 0; j < elements.length; j++) {
			if (i != j) {
				if (elements[i]['@attributes'].batname == elements[j]['@attributes'].batname
					&& elements[i]['@attributes'].type == elements[j]['@attributes'].type
					) {
					if (elements[i]['@attributes'].unit != '0') {
						elements[i]['@attributes'].unit = '' + (parseInt(elements[i]['@attributes'].unit)+1);
						elements[j]['@attributes'].unit = '0';
						positionToDelete.push(j);
					}
				}
			}
		}
	}
	positionToDelete.sort(function(a, b) {
		return b - a;
	});
	for (var i = 0; i < positionToDelete.length; i++) {
		elements.splice(positionToDelete[i],1);
	}

	//console.log(positionToDelete);
	return elements;
}

function getCampusElementsForEtage() {
	var elements = getCampusElements();
	var positionToDelete = Array();
	for (var i = 0; i < elements.length; i++) {
		for (var j = 0; j < elements.length; j++) {
			if (i != j) {
				if (elements[i]['@attributes'].batname == elements[j]['@attributes'].batname
					&& elements[i]['@attributes'].etagename == elements[j]['@attributes'].etagename
					&& elements[i]['@attributes'].type == elements[j]['@attributes'].type
					) {
					if (elements[i]['@attributes'].unit != '0') {
						elements[i]['@attributes'].unit = '' + (parseInt(elements[i]['@attributes'].unit)+1);
						elements[j]['@attributes'].unit = '0';
						positionToDelete.push(j);
					}
				}
			}
		}
	}
	positionToDelete.sort(function(a, b) {
		return b - a;
	});
	for (var i = 0; i < positionToDelete.length; i++) {
		elements.splice(positionToDelete[i],1);
	}

	//console.log(positionToDelete);
	return elements;
}

function getCampusEtageRange() {
	var resultat = new Array();

	var batTab = JSON.parse(window.localStorage.getItem('fullXML')).listbatiment.batiment;

	for (var i = 0; i < batTab.length; i++) {
		

		var prefix = batTab[i]['@attributes'].prefix;

		var etages = batTab[i].listetage.etage;
		if (etages.length > 0) {
			for (var j = 0; j < etages.length; j++) {
				var resultatAdd = new Array();

				resultatAdd.push(batTab[i]['@attributes'].nom);
				resultatAdd.push(prefix);
				resultatAdd.push(etages[j]['@attributes'].nom);
				resultatAdd.push(traiterEtageRange(etages[j]));

				resultat.push(resultatAdd);
			}
		} else {
			var resultatAdd = new Array();

			resultatAdd.push(batTab[i]['@attributes'].nom);
			resultatAdd.push(prefix);
			resultatAdd.push(etages['@attributes'].nom);
			resultatAdd.push(traiterEtageRange(etages));

			resultat.push(resultatAdd);
		}
		
	}
	return resultat;
}

function getCentre(batname, etagename) {
	var batiments = campusXML.getElementsByTagName('batiment');
    for (var i = 0; i < batiments.length; i++) {
        if (batiments[i].attributes.nom.value == batname) {
            var etages = batiments[i].getElementsByTagName('etage');
            for (var j = 0; j < etages.length; j++) {
                if (etages[j].attributes.nom.value == etagename) {
                	console.log(etages[j].getElementsByTagName('centre'));
                	return [parseFloat(etages[j].getElementsByTagName('centre')[0].attributes.y.value),
                	parseFloat(etages[j].getElementsByTagName('centre')[0].attributes.x.value)]
                }
            }
        }
    }
}