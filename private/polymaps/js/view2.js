﻿function designView() {
	document.querySelector('#etages').innerHTML = '';
	var elementss = getCampusElementsForEtage();

	var elements = new Array();
	var currentbat = window.localStorage.getItem('currentbat');


	// on va filtrer les éléments pour ne retenir que ceux qui appartiennent au batiment
	for (var i = 0; i < elementss.length; i++) {
		if (elementss[i]['@attributes'].batname == currentbat) {
			elements.push(elementss[i]);
		}
	}
	
	// maintenant on a plus que les éléments du batiment en question
	// --> on regroupe par étage maintenant
	var elementsParEtage = new Array(); // autant de case qu'il y a d'étages

	for (var i = 0; i < elements.length; i++) {
		var element = elements[i];

		var isEtage = false;
		// on parcourt les étages de elementsParEtage
		for (var j = 0; j < elementsParEtage.length; j++) {
			// on parcourt chaque élément de l'étage
			for (var k = 0; k < elementsParEtage[j].length; k++) {
				if (element['@attributes'].etagename == elementsParEtage[j][k]['@attributes'].etagename) {
					elementsParEtage[j].push(element);
					isEtage = true;
					break;
				}
			}
		}
		if (!isEtage) {
			var tempArray = new Array();
			tempArray.push(element);
			elementsParEtage.push(tempArray);
		}
	}

	// maintenant elementsParEtage contient autant de case que d'objet
	for (var i = 0; i < elementsParEtage.length; i++) {

		
		var toaddbis = '';
		var range = getCampusEtageRange();
		var numeroEtage = '';
		
		
		for (var j = 0; j < elementsParEtage[i].length; j++) {
			var nom = elementsParEtage[i][j]['@attributes'].type;
			numeroEtage = elementsParEtage[i][j]['@attributes'].etagename;
			toaddbis +="<td><span class='icons' style='background-image:url(images/menubar/"+nom.replace(/ /g,'')+".png)' title='"+nom+"'></span><span class='icons-text'>x" +elementsParEtage[i][j]['@attributes'].unit + "</span></td>";
			if(j%2 == 1) toaddbis += "<td></td></tr><tr><td></td>";
		}
		var toadd = '<div class="col-sx-4 col-ms-4 col-md-4 theblock">';
		toadd += '<div onclick="goEtage(\''+numeroEtage+'\')" class="etage img-thumbnail col-md-12"><h1>Etage ';
		toadd += numeroEtage + '</h1>';
		
		
		for(var k = 0; k < range.length;k++) {
			if(currentbat == range[k][0] && range[k][2] == i+1) {
				toadd += "<small>Salles : "+range[k][1]+range[k][3][0];
				toadd += " - "+range[k][1]+range[k][3][1]+"</small><hr/><table class='table borderless'>";
				
			}
		}
			
		var cpt = 0;
		for ( var j = 0; j < locations.length; j++) {
			if (locations[j][2] == window.localStorage.getItem('currentbat') && locations[j][3] == numeroEtage) {
				cpt++
			}
		}
		toadd += "<tr><td><span class=' text-left icons icon-personne' title='personnes présentes'></span><span class='icons-text'>x" + cpt+ "</td><td></td><td></td>";
		toadd += "<td><span class='icons icon-moi' title='Ma localisation'></span></td></tr><tr><td></td>";
		toadd += toaddbis;

		
		toadd += '</table></div></div>';
		document.querySelector('#etages').innerHTML += toadd;
	}

}

function goEtage(numEtage) {
	window.localStorage.setItem('currentetage',numEtage);
	document.location.href = './View3.html';
}