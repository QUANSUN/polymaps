﻿var map;
var testLayer;
var popupLayer;
var lcontrol;
var layerJSON;
var layerGroup = L.layerGroup();
var layerGroupP = L.layerGroup();
var timeouts = new Array();

var touchstartX = 0;
function initmap(campusPoint) {
	isMobile();
    var batname = window.localStorage.getItem('currentbat');

    var southWest = L.latLng(campusPoint[0][0], campusPoint[0][1]),
        northEast = L.latLng(campusPoint[1][0], campusPoint[1][1]),
        bounds = L.latLngBounds(southWest, northEast);
    var draggable = true;
    try {
        if (window.localStorage.getItem('listsalles') == null) {
            draggable = true;
        } else {
            draggable = false;
        }
    } catch (err) {draggable = true;}
    //Creation
    console.log(window.localStorage.getItem('listsalles'));
	if(mobile) {
        map = L.map('map', {
            center: [43.61567, 7.07222],
    		zoom: 18,
    		minZoom: 18,
            maxZoom: 21,
            dragging : draggable,
    		maxBounds : bounds,
    		zoomControl: false,
        });
	} else {
    	map = L.map('map', {
            center: [43.61567, 7.07222],
    		zoom: 18,
    		minZoom: 18,
            maxZoom: 20,
    		maxBounds : bounds,
    		zoomControl: false,
    		scrollWheelZoom: false,
        });
	}
    var centre = getCentre(window.localStorage.getItem('currentbat'),window.localStorage.getItem('currentetage'));

    // add an OpenStreetMap tile layer
    L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
    }).addTo(map);

    var zoomEff = 19;
	
	if(mobile) {
        locateMobile();
        var sallesSelected = window.localStorage.getItem('listsalles');

        if (sallesSelected != '') {

            try {
                var tabsalles = JSON.parse(sallesSelected);

                for (var h = 0; h < tabsalles.length; h++) {
                    if (tabsalles[h][4] == true) {
                        console.log('affichage');
                        window.localStorage.setItem('currentbat',tabsalles[h][0]);
                        window.localStorage.setItem('currentetage',tabsalles[h][1]);

                        var points = search('//batiment[@nom = "'+tabsalles[h][0]
                            +'"]//etage[@nom = "'+tabsalles[h][1]+'"]//salle[@nom = "'+tabsalles[h][2]+'"]//point', campusXML);

                        var pointArrayp = new Array();
                        for (var l = 0; l < points.length; l++) {
                            try {points[l].parentNode.attributes.nom.value;
                            pointArrayp.push([parseFloat(points[l].attributes.y.value),
                                            parseFloat(points[l].attributes.x.value)]);
                            } catch (err) {}
                        }
                        var papa = L.polygon(pointArrayp,{
                            color:'red',
                            fillOpacity:0.8,
                            weight:5,
                            opacity:0.2
                        }).addTo(map);
                                            
                        centre = (tabsalles[h][3]);
                    }
                }

                /** afficher le swipe **/
                
                document.querySelector('#swipe').style.display = 'block';
                setTimeout(function(){
                    document.querySelector('#swipe').style.opacity = '1';
                },500);
                setTimeout(function(){
                    document.querySelector('#swipe').style.opacity = '0';
                    setTimeout(function(){
                        document.querySelector('#swipe').style.display = 'none';
                    },1000);
                },2000);

            } catch (err) {/* Si on arrive ici c'est qu'il n'y a rien à voir */}
        }
        document.querySelector('#map').addEventListener('touchstart', function(e) {
            touchstartX = e.touches[0].pageX;
        });
        document.querySelector('#map').addEventListener('touchend', function(e) {
            if (touchstartX - e.changedTouches[0].pageX > window.innerWidth*0.4) {

                try {
                    var tabsalles = JSON.parse(sallesSelected);

                    for (var i = 0; i < tabsalles.length; i++) {
                        if (tabsalles[i][4] == true) {
                            // si i+1 existe, on peut passer à la salle d'après
                            try {
                                tabsalles[i+1][4] = true;
                                tabsalles[i][4] = false;
                                window.localStorage.setItem('listsalles',JSON.stringify(tabsalles));
                                window.location.href = './View3.html';
                                break;
                            }
                            //sinon rien ne se passe
                            catch (err) {}
                        }
                    }
                } catch (err) {/* Si on arrive ici c'est qu'il n'y a rien à voir */}
            }
            if (e.changedTouches[0].pageX - touchstartX > window.innerWidth*0.4) {
                try {
                    var tabsalles = JSON.parse(sallesSelected);
                    for (var i = 0; i < tabsalles.length; i++) {
                        if (tabsalles[i][4] == true) {
                            // si i-1 existe, on peut passer à la salle d'après
                            try {
                                tabsalles[i-1][4] = true;
                                tabsalles[i][4] = false;
                                window.localStorage.setItem('listsalles',JSON.stringify(tabsalles));
                                window.location.href = './View3.html';
                                break;
                            }
                            //sinon rien ne se passe
                            catch (err) {}
                        }
                    }
                } catch (err) {/* Si on arrive ici c'est qu'il n'y a rien à voir */}
            }
        });
    } else {
        zoomEff = 20;
        map.on('click', function(e) {
            document.querySelector('#menuBracetag').style.display = 'none';
        });
        map.on('drag', function(e) {
        	document.querySelector('#menuBracetag').style.display = 'none';
        });
        var positionSearched = window.localStorage.getItem('position');
        try {
            if (positionSearched != '') {
                window.localStorage.setItem('position','');
                var tempa = positionSearched.split(',');
                var marker = L.marker([parseFloat(tempa[0])-0.00002,parseFloat(tempa[1]) ]).addTo(map);
                marker.bindPopup('La salle recherchée !');
            }
        } catch (err) {}
        document.querySelector('#bracetagName').addEventListener('keyup',function(e) {

            if (e.which == 13) {
                ajouterTagModal();
            }
        });
	}

    
    

    displayElements();
    drawSalles(window.localStorage.getItem('currentbat'));
    displayEscaliers(window.localStorage.getItem('currentbat'),window.localStorage.getItem('currentetage'));
	
	setTimeout( function() {
	   map.setView(centre, zoomEff);
	}, 300);

    $('#modalAjoutBracetag').on('hidden.bs.modal', function () {
        document.querySelector('#loading-result').style.visibility = 'hidden';
        document.querySelector('#bracetagName').value = '';
    });
}

function displayEscaliers(batname, etagename) {
    var escaliers = search('//batiment[@nom = "'+ batname +'"]//etage[@nom = "'+etagename+'"]//escalier', campusXML);
    for (var k = 0; k < escaliers.length; k++) {
        if (parseInt(escaliers[k].attributes.etagedestination.value) > parseInt(etagename)) {
            drawEscalierT(escaliers[k].getElementsByTagName('point')[0].attributes,escaliers[k].attributes.etagedestination.value,'upescalier');
        } else {
            drawEscalierT(escaliers[k].getElementsByTagName('point')[0].attributes,escaliers[k].attributes.etagedestination.value,'downescalier');
        }
    }            
}

var escalierlayer = L.layerGroup();
function drawEscalierT(point, etagedestination, upOrDown) {
    var y = point.y.value;
    var x = point.x.value;
    var greenIcon = L.icon({
        iconUrl: './images/ionicon/'+upOrDown+'.png',
        iconSize:     [mobile? 15:30, mobile? 15:30], // size of the icon
        iconAnchor:   [mobile? 7.5:15, mobile? 7.5:15], // point of the icon which will correspond to marker's location
    });
    var marker = L.marker([y, x], {icon: greenIcon});
    if (mobile) {
        var textPopup = "";
        textPopup += "<div ";
        textPopup += "class='popupMobile'";
        textPopup += ">"+'Escalier destination : '+ etagedestination;
        textPopup += "</div>";
        marker.bindPopup(textPopup);
    } else {
        marker.on('mouseover',function () {
            document.querySelector('#infoSalle').innerHTML = 'Escalier destination : '+ etagedestination;
            document.querySelector('#infoSalle').style.opacity = "1";
        });
        marker.on('mouseout',function () {
            document.querySelector('#infoSalle').style.opacity = "0";
        });
        marker.on('click', function () {
            window.localStorage.setItem('currentetage',etagedestination);
            window.location.href = './View3.html';
        });
    }
    escalierlayer.addLayer(marker);
}

function displayElements() {
    var elements = search('//batiment[@nom = "'+ window.localStorage.getItem('currentbat') +'"]//etage[@nom = "'+window.localStorage.getItem('currentetage')+'"]//element', campusXML);
    for (var k = 0; k < elements.length; k++) { drawElement(elements[k]); }           
}

var typeStr = new Array();
var typeLayer = new Array();

function drawElement(element) {
    var type = element.attributes.type.value;
    var x = element.getElementsByTagName('point')[0].attributes.x.value;
    var y = element.getElementsByTagName('point')[0].attributes.y.value;
    var greenIcon = L.icon({
        iconUrl: './images/menubar/'+type.replace(/ /g,'')+'.png',
        iconSize:     [mobile? 25:40, mobile? 25:40], // size of the icon
        iconAnchor:   [mobile? 12.5:20, mobile? 12.5:20], // point of the icon which will correspond to marker's location
    });
    var marker = L.marker([y, x], {icon: greenIcon});
    if (mobile) {
        var textPopup = "";
        textPopup += "<div ";
        textPopup += "class='popupMobile'";
        textPopup += ">"+element.attributes.type.value;
        textPopup += "</div>";
        marker.bindPopup(textPopup);
    } else {
        marker.on('mouseover',function () {
            document.querySelector('#infoSalle').innerHTML = element.attributes.type.value;
            document.querySelector('#infoSalle').style.opacity = "1";
        });
        marker.on('mouseout',function () {
            document.querySelector('#infoSalle').style.opacity = "0";
        });
    }

    var index = typeStr.indexOf(element.attributes.type.value);
    if (index >= 0) {
        // on l'ajoute dans le 2ème tableau
        typeLayer[index].addLayer(marker);
    } else {
        // on le créé les 2 tableaux
        typeStr.push(element.attributes.type.value);

        var layerInArray = L.layerGroup();
        layerInArray.addLayer(marker);
        typeLayer.push(layerInArray);
    }
}

function drawSalles(batname) {
    var lcontrol;

    /* drawing the salles */
    var salles = search('//batiment[@nom = "'+ batname +'"]//etage[@nom = "'+window.localStorage.getItem('currentetage')+'"]//salle', campusXML);
    for (var k = 0; k < salles.length; k++) {
        drawSalle(salles[k]);
    }

    /* drawing the batiment contour */
    var points = search('//batiment[@nom = "'+ batname +'"]//etage[@nom = "'+window.localStorage.getItem('currentetage')+'"]', campusXML)[0].childNodes;
    var etagePoints = new Array();
    for (var i = 0; i < points.length; i++) {
        if (points[i].nodeName == 'point') {
            var resultTab = new Array();
            resultTab.push(parseFloat(points[i].attributes.y.value));
            resultTab.push(parseFloat(points[i].attributes.x.value));
            etagePoints.push(resultTab);
        }
    }
    var polygon = L.polygon(etagePoints,{
        color:'red',
        fillOpacity:0,
        weight:5,
        opacity:0.5
    }).addTo(map);
    
    /* drawing the batiment doors */
    var portes = search('//batiment[@nom = "'+ batname +'"]//etage[@nom = "'+window.localStorage.getItem('currentetage')+'"]/listporte//porte', campusXML);
    for (var k = 0; k < portes.length; k++) {
        var xt = portes[k].getElementsByTagName('point')[0].attributes.x.value;
        var yt = portes[k].getElementsByTagName('point')[0].attributes.y.value;
        var circle = L.circle([yt, xt], 0.5, {
            color: 'red',
            fillColor: '#f03',
            fillOpacity: 0.5
        });
        ajoutPorteLayout(circle);
    }
	
	layerGroup.addTo(map);
	layerGroupP.addTo(map);
	var overlayMaps = {
    "Salle": layerGroup,
	"Porte": layerGroupP
	};
    for (var i = 0; i < typeStr.length; i++) {
        overlayMaps[typeStr[i]] = typeLayer[i];
        typeLayer[i].addTo(map);
    }
    overlayMaps['Escaliers'] = escalierlayer;
    escalierlayer.addTo(map);
	lcontrol = L.control.layers(null, overlayMaps, true);
	lcontrol.addTo(map);
}

function drawSalle(salle) {
	
    var salleXML = salle.childNodes;
    var result = new Array();
    for (var i = 0; i < salleXML.length; i++) {
        if (salleXML[i].nodeName == 'point') {
            var resultTab = new Array();
            resultTab.push(parseFloat(salleXML[i].attributes.y.value));
            resultTab.push(parseFloat(salleXML[i].attributes.x.value));
            result.push(resultTab);
        }
    }
	
	var currentSalle = L.polygon(result);
	ajoutSalleLayout(currentSalle, salle.attributes.nom.value, salle.attributes.capacity.value);
		
    var portes = salle.getElementsByTagName('porte');
    for (var i = 0; i < portes.length; i++) {
        var xt = portes[i].getElementsByTagName('point')[0].attributes.x.value;
        var yt = portes[i].getElementsByTagName('point')[0].attributes.y.value;
        var circle = L.circle([yt, xt], 0.35, {
            color: 'green',
            fillColor: 'green',
            fillOpacity: 0.8
        });
		ajoutPorteLayout(circle);
    }	
}

function ajoutPorteLayout(porte) { layerGroupP.addLayer(porte); }

function ajoutSalleLayout(salle, nom, capacite) {
	var textPopup;
	var podium = getPodium(window.localStorage.getItem('currentbat'), window.localStorage.getItem('currentetage'), nom);
	var topThree = "<p class='bracelist'>";
	for(var i=0; i< podium.length;i++) {
			topThree += "<img src='images/medal-"+i+".png' class='medal'/><span class='brace'>{</span>"+podium[i][0]+"<span class='brace'>}</span>";
			if(i < podium.length-1) topThree += ", ";
			else topThree += "</p>";
		}
		
	if(mobile) {
		textPopup = "<div class='popupMobile' data-toggle='modal' data-target='#modalVoteBracetag'>Salle : "+nom+"<br/>Nombre de place : "+capacite+topThree+"</div>";
		salle.bindPopup(textPopup);
		
		salle.on('click', function(e) {
            	var voteTab = createTab(nom);
				
				document.querySelector('#tabBraceTag').innerHTML = voteTab;
                addMobileListenersTable(nom);
        });
    } else {
		textPopup = "<p>Salle : "+nom+"<br/>Nombre de place : "+capacite+"<br/></p>"+topThree;		
    	salle.on('mouseover', function (e) {
    		 document.querySelector('#infoSalle').innerHTML = textPopup;
             document.querySelector('#infoSalle').style.opacity = "1";
    	});
        salle.on('mouseout',function () {
            document.querySelector('#infoSalle').style.opacity = "0";
        });
		salle.on('click', function(e) {
                document.querySelector('#menuBracetag').innerHTML = "<table class='table table-bordered table-hover menu'><tr data-toggle='modal' data-target='#modalAjoutBracetag'><td><span class='glyphicon glyphicon-plus menu-icon'></span> <span class='menu-text'>{Bracetag}</span></td></tr><tr data-toggle='modal' data-target='#modalVoteBracetag'><td><span class='glyphicon glyphicon-thumbs-up menu-icon'></span> <span class='menu-text'>{Bracetag}</span></td></tr></table>";
                document.querySelector('#menuBracetag').style.display = 'block';
                document.querySelector('#menuBracetag').style.top = e.originalEvent.pageY + 'px';
                document.querySelector('#menuBracetag').style.left = e.originalEvent.pageX + 'px';
				document.querySelector('#salle').value = nom;
				
				var voteTab = createTab(nom);
				
				document.querySelector('#tabBraceTag').innerHTML = voteTab;
                addPCListenersTable();
        });
    }
	layerGroup.addLayer(salle);
}

function createTab(nom) {
	var result="";
	var allTag = getAllTagSalle(window.localStorage.getItem('currentbat'), window.localStorage.getItem('currentetage'), nom);
	
	for(var i=0; i<allTag.length;i++) {
		result += "<tr class='bracelist'><td><span class='brace'>{</span><span>"
        +allTag[i][0]+"</span><span class='brace'>}</span></td>"
        +"<td><img src='images/yeah.png' class='yeah-bouh'/><span> "
        +allTag[i][1]+" </span></td><td><img src='images/bouh.png' class='yeah-bouh'/><span> "
        +allTag[i][2]+" </span></td></tr>";
	
	}
	return result;
}

function ajouterTagModal() {
    document.querySelector('#loading-result').src = './images/loading.gif';
    document.querySelector('#loading-result').style.visibility = 'visible';
    try {
        var tag = document.querySelector('#bracetagName').value.replace(/[^a-zA-Z0-9]/g,'');
        
    	if(tag.length <= 15 && tag.length > 0) {
            var batname = window.localStorage.getItem('currentbat');
            var etagename = window.localStorage.getItem('currentetage');
            var sallename = document.querySelector('#salle').value;
            if (window.localStorage.getItem(batname+':'+etagename+':'+sallename+':'+tag) != 'OK') {
                ajouterTag(batname, etagename, sallename, tag);
            } else {
                document.querySelector('#loading-result').src = './images/close.png';
                document.querySelector('#loading-result').title = 'Vous avez déjà ajouté/voté ce {BraceTag}';
            }
        } else {
            document.querySelector('#loading-result').src = './images/close.png';
            document.querySelector('#loading-result').title = 'vous devez saisir au moins 1 caractères et moins de 15 caractères';
        }

    } catch (err) {
        document.querySelector('#loading-result').src = './images/close.png';
        document.querySelector('#loading-result').title = 'vous devez saisir au moins 1 caractères et moins de 15 caractères';
    }
}

function addPCListenersTable() {
    var tds2 = document.querySelectorAll('#tabBraceTag tr td:nth-child(2)');
    for (var i = 0; i < tds2.length; i++) {
        tds2[i].addEventListener('mousedown',function(e) {
            var batname = window.localStorage.getItem('currentbat');
            var etagename = window.localStorage.getItem('currentetage');
            var sallename = document.querySelector('#salle').value;
            var tagname = this.parentNode.getElementsByTagName('td')[0].getElementsByTagName('span')[1].innerHTML;
            if (window.localStorage.getItem(batname+':'+etagename+':'+sallename+':'+tagname) != 'OK') {
                initPlusUn(e);
                this.innerHTML = '<img src="images/yeah.png" class="yeah-bouh"/><span> '+(parseInt(this.getElementsByTagName('span')[0].innerHTML)+1)+' </span>';
                incrementeTag(batname,etagename,sallename,tagname,'yeah');
            }
        });
    }
    var tds3 = document.querySelectorAll('#tabBraceTag tr td:nth-child(3)');
    for (var i = 0; i < tds3.length; i++) {
        tds3[i].addEventListener('mousedown',function(e) {
            var batname = window.localStorage.getItem('currentbat');
            var etagename = window.localStorage.getItem('currentetage');
            var sallename = document.querySelector('#salle').value;
            var tagname = this.parentNode.getElementsByTagName('td')[0].getElementsByTagName('span')[1].innerHTML;
            if (window.localStorage.getItem(batname+':'+etagename+':'+sallename+':'+tagname) != 'OK') {
                initPlusUn(e);
                this.innerHTML = '<img src="images/bouh.png" class="yeah-bouh"/><span> '+(parseInt(this.getElementsByTagName('span')[0].innerHTML)+1)+' </span>';
                incrementeTag(batname,etagename,sallename,tagname,'bouh');
            }
        });
    }
}

function addMobileListenersTable(salle) {
    var tds2 = document.querySelectorAll('#tabBraceTag tr td:nth-child(2)');
    for (var i = 0; i < tds2.length; i++) {
        tds2[i].addEventListener('touchstart',function(e) {
            var batname = window.localStorage.getItem('currentbat');
            var etagename = window.localStorage.getItem('currentetage');
            var tagname = this.parentNode.getElementsByTagName('td')[0].getElementsByTagName('span')[1].innerHTML;
            if (window.localStorage.getItem(batname+':'+etagename+':'+salle+':'+tagname) != 'OK') {
                initPlusUn(e);
                this.innerHTML = '<img src="images/yeah.png" class="yeah-bouh"/><span> '+(parseInt(this.getElementsByTagName('span')[0].innerHTML)+1)+' </span>';
                incrementeTag(batname,etagename,salle,tagname,'yeah');
            }
        });
    }
    var tds3 = document.querySelectorAll('#tabBraceTag tr td:nth-child(3)');
    for (var i = 0; i < tds3.length; i++) {
        tds3[i].addEventListener('touchstart',function(e) {
            var batname = window.localStorage.getItem('currentbat');
            var etagename = window.localStorage.getItem('currentetage');
            var tagname = this.parentNode.getElementsByTagName('td')[0].getElementsByTagName('span')[1].innerHTML;
            if (window.localStorage.getItem(batname+':'+etagename+':'+salle+':'+tagname) != 'OK') {
                initPlusUn(e);
                this.innerHTML = '<img src="images/bouh.png" class="yeah-bouh"/><span> '+(parseInt(this.getElementsByTagName('span')[0].innerHTML)+1)+' </span>';
                incrementeTag(batname,etagename,salle,tagname,'bouh');
            }
        });
    }
}

function initPlusUn(e) {
    clearTimeout(timeout);
    document.querySelector('#un').style.color = "rgb("+Math.floor(((Math.random()*256)+200)/2)+","+Math.floor(((Math.random()*256)+220)/2)+","+Math.floor(((Math.random()*256)+180)/2)+")";
    document.querySelector('#un').style.display = 'block';
    document.querySelector('#un').style.transition = '';
    if (mobile) {
        document.querySelector('#un').style.top = (e.touches[0].pageY+10) + 'px';
        document.querySelector('#un').style.left = (e.touches[0].pageX) + 'px';
    } else {
        document.querySelector('#un').style.top = (e.pageY+10) + 'px';
        document.querySelector('#un').style.left = (e.pageX) + 'px';
    }
    document.querySelector('#un').style.opacity = '1';
    document.querySelector('#un').style.transform = 'rotate(0deg)';
    setTimeout(function() {
        movePlusUn();
    },100);
}

var timeout;
function movePlusUn() {
    document.querySelector('#un').style.transition = 'all 1.5s';
    document.querySelector('#un').style.top = (parseInt(document.querySelector('#un').style.top) 
    + (Math.random() > 0.5 ? -1 : 1)*Math.random()*200) + 'px';
    document.querySelector('#un').style.left = (parseInt(document.querySelector('#un').style.top) 
    + (Math.random() > 0.5 ? -1 : 1)*Math.random()*200) + 'px';
    document.querySelector('#un').style.opacity = '0';
    document.querySelector('#un').style.transform = 'rotate('
        + (Math.random() > 0.5 ? -1 : 1)*Math.random()*720 +'deg)';
    timeout = setTimeout(function() {document.querySelector('#un').style.display = 'none';},1500);

}