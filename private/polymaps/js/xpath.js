﻿function parseXml(xmlStr) {
    if (window.DOMParser) {
        return ( new window.DOMParser() ).parseFromString(xmlStr, "text/xml");
    } else if (typeof window.ActiveXObject != "undefined" && new window.ActiveXObject("Microsoft.XMLDOM")) {
        var xmlDoc = new window.ActiveXObject("Microsoft.XMLDOM");
        xmlDoc.async = "false";
        xmlDoc.loadXML(xmlStr);
        return xmlDoc;
    } else { return null; }
}

function search(xpath, docXml) {
    var nodes = new Array();
    var iterator;
    /* Not For IE */
    try { iterator = docXml.evaluate(xpath,docXml,null,XPathResult.UNORDERED_NODE_ITERATOR_TYPE,null); }
    catch (err) {
        iterator = document.evaluate(xpath,document,null,XPathResult.UNORDERED_NODE_ITERATOR_TYPE,null);
        console.debug('docXML doesn\'t work !');
    }
    try {
        var thisNode = iterator.iterateNext();
        while (thisNode) { nodes.push(thisNode); thisNode = iterator.iterateNext(); }   
    } catch (e) { console.debug('Error: Document tree modified during iteration ' + e); }

    return nodes;
}