﻿var locations = [
	['Jean-Yves',[43.61567, 7.07222],'Ouest','1'],
	['Maxime',[43.61590, 7.07245],'Est1','1'],
	['Quan',[43.61620, 7.07145],'Campus',''],
	['Amir',[43.61525, 7.07275],'Sud','1'],
	['Jérémy',[43.61640, 7.07265],'Campus','']
];

var locationParBatiment = [
	[
		['Jean-Yves',[43.61567, 7.07222],'Ouest','2']
	],
	[
		['Maxime',[43.61590, 7.07245],'Est1','1']
	],
	[
		['Amir',[43.61525, 7.07275],'Sud','1']
	],
	[
		['Jérémy',[43.61640, 7.07265],'Campus',''],
		['Quan',[43.61620, 7.07145],'Campus','']
	]
];

function randomChange() {
	for (var i = 0; i < locations.length; i++) {
		var randomInt = Math.random();
		if (randomInt < 0.20) {
			locations[i][1][0] += Math.random()/50000;
			locations[i][1][1] += Math.random()/50000;
		} else if (randomInt < 0.4) {
			locations[i][1][0] -= Math.random()/50000;
			locations[i][1][1] -= Math.random()/50000;
		} else if (randomInt < 0.6) {
			locations[i][1][0] += Math.random()/50000;
			locations[i][1][1] -= Math.random()/50000;
		} else if (randomInt < 0.8) {
			locations[i][1][0] -= Math.random()/50000;
			locations[i][1][1] += Math.random()/50000;
		}
	}
}