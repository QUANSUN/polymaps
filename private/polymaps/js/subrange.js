﻿function traiterEtageRange(etage) {
	var resultat = new Array();
	var salles = etage.listsalle.salle;
	if (salles.length > 0) {
		var max = 0;
		var min = 500;
		for (var i = 0; i < salles.length; i++) {
			if (parseInt(salles[i]['@attributes'].nom) >= 0) {
				if (parseInt(salles[i]['@attributes'].nom) > max) {
					max = parseInt(salles[i]['@attributes'].nom);
				}
				if (parseInt(salles[i]['@attributes'].nom) < min) {
					min = parseInt(salles[i]['@attributes'].nom);
				}
			}
		}
		resultat.push(min);
		resultat.push(max);
	} else {
		resultat.push(parseInt(salles['@attributes'].nom)); // min
		resultat.push(parseInt(salles['@attributes'].nom)); // max
	}
	return resultat;
}