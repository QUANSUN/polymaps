﻿/**
	validation du nouveau XML par le php
**/
function getValidation(fonction) {
	var xdr = getXDomainRequest();
     
	xdr.onload = fonction;
	xdr.open("POST", "./php/validateXML.php", true);
	xdr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	var xmlString = new XMLSerializer().serializeToString(campusXML);
	xdr.send('xmlstring='+xmlString);
}

/**
	ajoute le tag dans le xml temporaire et l'envoie au php pour validation
	la réponse du php se fait dans le fonction getValidation d'au dessus

	Si le tag est déjà présent dans le XML 
	ALORS on incrémente le yeah du tag que l'on voulait ajouter
**/
function ajouterTag(batname, etagename, sallename, tagname) {
    if (window.localStorage.getItem(batname+':'+etagename+':'+sallename+':'+tagname) == 'OK') {
    } else {
        var thistag = search('//batiment[@nom = "'+batname
                                +'"]//etage[@nom = "'+etagename
                                +'"]//salle[@nom = "'+sallename+'"]//bracetag[@nom = "'+tagname+'"]', campusXML);
        console.log(thistag);
        if (thistag.length > 0) {
            incrementeTag(batname, etagename, sallename, tagname, 'yeah');
            document.querySelector('#loading-result').src = './images/check.png';
            document.querySelector('#bracetagName').value = '';
            return;
        } else {
            var listtag = search('//batiment[@nom = "'+batname
                                +'"]//etage[@nom = "'+etagename
                                +'"]//salle[@nom = "'+sallename+'"]//listtag', campusXML);

            var newChild = '<bracetag nom="' + tagname + '" yeah="0" bouh="0"/>';
            var div = document.createElement('div');
            div.innerHTML = newChild;
            var newElement = div.firstChild;

            listtag[0].appendChild(newElement);

            getValidation(function (reponse) {
                reponse = reponse.currentTarget.responseText;
                // REPONSE A MODIFIER ICI 
                if (reponse == 'validate') {
                    window.localStorage.setItem(batname+':'+etagename+':'+sallename+':'+tagname,'OK');
                    document.querySelector('#loading-result').src = './images/check.png';
                    document.querySelector('#bracetagName').value = '';
                } else {
                    document.querySelector('#loading-result').src = './images/close.png';
                    document.querySelector('#loading-result').title = 'Erreur du serveur interne.';
                    console.error(reponse);
                }
            });

        }
    }
}

/**
	retourne un tableau qui contient tous les tags
	un tag est un tableau de 3 case
		première case : nom du tag
		deuxième case : nombre de yeah
		troisième case : nombre de bouh
**/
function getAllTagSalle(batname, etagename, sallename) {
	var result = new Array();

    var thistag = search('//batiment[@nom = "'+batname
                                +'"]//etage[@nom = "'+etagename
                                +'"]//salle[@nom = "'+sallename+'"]//bracetag', campusXML);

    for (var l = 0; l < thistag.length; l++) {
        var resultTag = new Array();
        resultTag.push(thistag[l].attributes.nom.value);
        resultTag.push(thistag[l].attributes.yeah.value);
        resultTag.push(thistag[l].attributes.bouh.value);
        result.push(resultTag);
    }

    result.sort(function (tag1, tag2) {
        return  (parseInt(tag2[1]) - parseInt(tag2[2])) - (parseInt(tag1[1]) - parseInt(tag1[2]));
    });
    return result;
}

/**
	incremente temporairement le tag chez le client et envoie une confirmation au serveur pour abbrobation
**/
function incrementeTag(batname, etagename, sallename, tagname, yeahOrBouh) {
    var thistag = search('//batiment[@nom = "'+batname
                                +'"]//etage[@nom = "'+etagename
                                +'"]//salle[@nom = "'+sallename+'"]//bracetag[@nom = "'+tagname+'"]', campusXML);
    if (yeahOrBouh == 'yeah') {
        thistag[0].attributes.yeah.value = parseInt(thistag[0].attributes.yeah.value)+1;
    } else if (yeahOrBouh == 'bouh'){
        thistag[0].attributes.bouh.value = parseInt(thistag[0].attributes.bouh.value)+1;
    }

    getValidation(function (reponse) {
        reponse = reponse.currentTarget.responseText;
        /* REPONSE A MODIFIER ICI */
        if (reponse == 'validate') {
            window.localStorage.setItem(batname+':'+etagename+':'+sallename+':'+tagname,'OK');
        } else {
            console.error(reponse);
        }
    });
}


/**
	retourne un tableau de tous les tags sous forme de string
**/
function getAllTagName() {
	var tags = campusXML.getElementsByTagName('bracetag');
	var resultat = new Array();
	for (var i = 0; i < tags.length; i++) {
		var tagname = tags[i].attributes.nom.value;
		if (resultat.indexOf(tagname) < 0) {
			resultat.push(tagname);
		}
	}
	resultat.sort();
	return resultat;
}

/**
	retourne les tags qui contient tagname dans l'ordre alphabétique
**/
function searchTag(tagname) {
	var tagnames = getAllTagName();
	var result = new Array();
	for (var i = 0; i < tagnames.length; i++) {
		if (tagnames[i].toLowerCase().indexOf(tagname.toLowerCase()) >= 0) {
			result.push(tagnames[i]);
		}
	}
	return result;
}

/** 
	TODO : FAIRE UNE FONCTION QUI RETOURNE UN tableau
	de tableau --> [batiment, etage, salle, coordonneeDuPins]
	qui prend en paramètre un tagname
**/
function getSallesByTagName(tagname) {
	var result = new Array();
	var batiments = campusXML.getElementsByTagName('batiment');
    for (var i = 0; i < batiments.length; i++) {
        var etages = batiments[i].getElementsByTagName('etage');
        for (var j = 0; j < etages.length; j++) {
            var salles = etages[j].getElementsByTagName('salle');
            for (var k = 0; k < salles.length; k++) {
            	var tags = salles[k].getElementsByTagName('bracetag');
                for (var l = 0; l < tags.length; l++) {
                	if (tags[l].attributes.nom.value == tagname) {
                		var pointDechet = salles[k].childNodes;
                		var maxX = 0;
                		var maxY = 0;
                		for (var m = 0; m < pointDechet.length; m++) {
                			try { // si c'est un point
	                			if (pointDechet[m].attributes.y.value > maxY) {
	                				maxY = pointDechet[m].attributes.y.value;
	                				maxX = pointDechet[m].attributes.x.value;
	                			}
	                		} catch (err) {} // sinon
                		}
                		result.push([
                			batiments[i].attributes.nom.value,
                			etages[j].attributes.nom.value,
                			salles[k].attributes.nom.value,
                			[maxY, maxX]
                		]);
                	}
                }
            }
        }
    }
    return result;
}

/** 
    retourne un tableau de maximum 3 cases avec le top 3 des tags pour une salle
    un élément du tableau est sous la forme :
        [nomDuTag, NombreDeYeah - NombreDeBouh]
**/
function getPodium(batname, etagename, sallename) {
    var tab = getAllTagSalle(batname, etagename, sallename);

    var result = new Array();
    var cpt = 0;
    for (var i = 0; i < tab.length; i++) {
        if (cpt < 3) {
            result.push([tab[i][0], parseInt(tab[i][1]) - parseInt(tab[i][2])]);
        } else {
            break;
        }
        cpt++
    }
    return result;
}