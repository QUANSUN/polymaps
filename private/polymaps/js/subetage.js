﻿function traiterBatiment(batiment,tabElement,batName) {
	try {
		if (batiment.listetage.etage.length > 0) {
			var etages = batiment.listetage.etage;
			for (var i = 0; i < etages.length; i++) {
				var etage = etages[i];
				traiterEtage(etage,tabElement,batName);
			}
		} else {
			var etage = batiment.listetage.etage;
			traiterEtage(etage,tabElement,batName);
		}
	} catch (err) {}
}

function traiterEtage(etage,tabElement,batName) {
	try {
		if (etage.listcouloir.couloir.length > 0) { // on a un tableau
			var couloirs = etage.listcouloir.couloir;
			for (var j = 0; j < couloirs.length; j++) {
				var couloir = couloirs[j];
				traiterCouloir(couloir,tabElement,etage['@attributes'].nom,batName)
			}
		} else { // on a un object
			var couloir = etage.listcouloir.couloir;
			traiterCouloir(couloir,tabElement,etage['@attributes'].nom,batName); // bug ici
		}
	} catch (err) {}

	try {
		if (etage.listsalle.salle.length > 0) {
			var salles = etage.listsalle.salle;
			for (var j = 0; j < salles.length; j++) {
				var salle = salles[j];
				traiterSalle(salle,tabElement,etage['@attributes'].nom,batName);
			}
		} else {
			var salle = etage.listsalle.salle;
			traiterSalle(salle,tabElement,etage['@attributes'].nom,batName);
		}
	} catch (err) {}
	
}

function traiterCouloir(couloir,tabElement,etageName,batName) {
	try {
		if (couloir.listelement.element.length > 0) {
			var elements = couloir.listelement.element;
			for (var k = 0; k < elements.length; k++) {
				var element = elements[k];
				traiterElement(element,tabElement,etageName,batName);
			}
		} else {
			var element = couloir.listelement.element;
			traiterElement(element,tabElement,etageName,batName);
		}
	} catch (err) {}
}

function traiterSalle(salle, tabElement,etageName,batName) {
	try {
		if (salle.listelement.element.length > 0) {
			var elements = salle.listelement.element;
			for (var k = 0; k < elements.length; k++) {
				var element = elements[k];
				traiterElement(element,tabElement,etageName,batName);
			}
		} else {
			var element = salle.listelement.element;
			traiterElement(element,tabElement,etageName,batName);
		}
	} catch (err) {}
}

function traiterElement(element,tabElement,etageName,batName) {
	element['@attributes'].etagename = etageName;
	element['@attributes'].batname = batName;
	element['@attributes'].unit = '1';
	tabElement.push(element);
}