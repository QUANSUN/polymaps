﻿function onEtageClick(event) {

    if (selected != false) {
        if (selected == 'porte') {
            var circle = L.circle(event.latlng, 0.5, {
                color: 'red',
                fillColor: '#f03',
                fillOpacity: 0.5
            });
            ajoutPorteLayout(circle);
            /** il faut l'ajouter dans le xml **/
            var batiments = campusXML.getElementsByTagName('batiment');
            for (var i = 0; i < batiments.length; i++) {
                if (batiments[i].attributes.nom.value == window.localStorage.getItem('currentbatadmin')) {
                    var etages = batiments[i].getElementsByTagName('etage');
                    for (var j = 0; j < etages.length; j++) {
                        if (etages[j].attributes.nom.value == window.localStorage.getItem('currentetageadmin')) {
                            var portes = etages[j].getElementsByTagName('listporte');
                            portes = portes[portes.length-1];
                            console.log(event.latlng.lng);
                            
                            var newChild = '<porte><point x="'+event.latlng.lng+'" y="'+event.latlng.lat+'"></point></porte>';
                            var div = document.createElement('div');
                            div.innerHTML = newChild;
                            var newElement = div.firstChild;
                            portes.appendChild(newElement);
                        }
                    }
                }
            }
        } else {
            var selectedWithoutSpace = selected.replace(/ /g,'');
            var greenIcon = L.icon({
                iconUrl: '../images/menubar/'+selectedWithoutSpace+'.png',
                iconSize:     [40, 40], // size of the icon
                iconAnchor:   [20, 20], // point of the icon which will correspond to marker's location
            });
            var marker = L.marker(event.latlng, {icon: greenIcon});
            
            

            var batiments = campusXML.getElementsByTagName('batiment');
            for (var i = 0; i < batiments.length; i++) {
                if (batiments[i].attributes.nom.value == window.localStorage.getItem('currentbatadmin')) {
                    var etages = batiments[i].getElementsByTagName('etage');
                    for (var j = 0; j < etages.length; j++) {
                        if (etages[j].attributes.nom.value == window.localStorage.getItem('currentetageadmin')) {
                            var elements = etages[j].getElementsByTagName('couloir')[0].getElementsByTagName('listelement')[0];
                            
                            var newChild = '<element type="'+selected+'"><point x="'+event.latlng.lng+'" y="'+event.latlng.lat+'"></element>';
                            var div = document.createElement('div');
                            div.innerHTML = newChild;
                            var newElement = div.firstChild;
                            elements.appendChild(newElement);
                            
                            var elementToadd = elements.getElementsByTagName('element');
                            typeLayer[typeStr.indexOf(elementToadd[elementToadd.length-1].attributes.type.value)].addLayer(marker);
                            marker.on('click',function() {
                                removeElement(elementToadd[elementToadd.length-1],marker);
                            });
                        }
                    }
                }
            }
        }
    }  
	
}

function onSalleClick(e,nom) {
    if (selected != false) {
        if (selected == 'porte') {
            var circle = L.circle(e.latlng, 0.35, {
                color: 'green',
                fillColor: 'green',
                fillOpacity: 0.8
            });
            ajoutPorteLayout(circle);
            // on a le nom de la salle, du batiment et de l'étage
            // il faut maintenant l'ajouter
            var batiments = campusXML.getElementsByTagName('batiment');
            for (var i = 0; i < batiments.length; i++) {
                if (batiments[i].attributes.nom.value == window.localStorage.getItem('currentbatadmin')) {
                    var etages = batiments[i].getElementsByTagName('etage');
                    for (var j = 0; j < etages.length; j++) {
                        if (etages[j].attributes.nom.value == window.localStorage.getItem('currentetageadmin')) {
                            var salles = etages[j].getElementsByTagName('salle');
                            for (var k = 0; k < salles.length; k++) {
                                if (salles[k].attributes.nom.value == nom) {
                                    var newChild = '<porte><point x="'+e.latlng.lng+'" y="'+e.latlng.lat+'"></point></porte>';
                                    var div = document.createElement('div');
                                    div.innerHTML = newChild;
                                    var newElement = div.firstChild;
                                    salles[k].getElementsByTagName('listporte')[0].appendChild(newElement);
                                }
                            }
                        }
                    }
                }
            }
        } else {
            var selectedWithoutSpace = selected.replace(/ /g,'');
            var greenIcon = L.icon({
                iconUrl: '../images/menubar/'+selectedWithoutSpace+'.png',
                iconSize:     [40, 40], // size of the icon
                iconAnchor:   [20, 20], // point of the icon which will correspond to marker's location
            });
            var marker = L.marker(e.latlng, {icon: greenIcon});
            

            var batiments = campusXML.getElementsByTagName('batiment');
            for (var i = 0; i < batiments.length; i++) {
                if (batiments[i].attributes.nom.value == window.localStorage.getItem('currentbatadmin')) {
                    var etages = batiments[i].getElementsByTagName('etage');
                    for (var j = 0; j < etages.length; j++) {
                        if (etages[j].attributes.nom.value == window.localStorage.getItem('currentetageadmin')) {
                            var salles = etages[j].getElementsByTagName('salle');
                            for (var k = 0; k < salles.length; k++) {
                                if (salles[k].attributes.nom.value == nom) {
                                    var newChild = '<element type="'+selected+'"><point x="'+e.latlng.lng+'" y="'+e.latlng.lat+'"></element>';
                                    var div = document.createElement('div');
                                    div.innerHTML = newChild;
                                    var newElement = div.firstChild;
                                    salles[k].getElementsByTagName('listelement')[0].appendChild(newElement);
                                    

                                    var elementToadd = salles[k].getElementsByTagName('listelement')[0].getElementsByTagName('element');
                                    typeLayer[typeStr.indexOf(elementToadd[elementToadd.length-1].attributes.type.value)].addLayer(marker);
                                    marker.on('click',function() {
                                        removeElement(elementToadd[elementToadd.length-1],marker);
                                    });
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

function removeElement(element,marker) {
    if (selected == false) {
        try {element.parentNode.removeChild(element);}
        catch(err) {}
        typeLayer[typeStr.indexOf(element.attributes.type.value)].removeLayer(marker);
    }
}