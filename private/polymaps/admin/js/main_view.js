﻿var map;
var testLayer;
var popupLayer;
var lcontrol;
var locateactived = true;
var popupactivated = true;
var randomColor = ['blue','red','green','black','orange','yellow','brown','grey','gray'];
var timeouts = new Array();

function initmap(campusPoint) {

	isMobile();
    window.localStorage.removeItem('listsalles');
	
    var southWest = L.latLng(campusPoint[0][0], campusPoint[0][1]),
        northEast = L.latLng(campusPoint[1][0], campusPoint[1][1]),
        bounds = L.latLngBounds(southWest, northEast);
    	
    //Creation
    map = L.map('map', {
        center: [43.61567, 7.07222],
        zoom: 18,
    	minZoom: 18,
    	maxBounds : bounds,
    	zoomControl: false,
    	dragging : false,
		doubleClickZoom: false
    });
	if(mobile) { locateMobile(); }
    
    // add an OpenStreetMap tile layer
    L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
    }).addTo(map);

    // add all content
    displayElement();

    document.addEventListener('keyup',function(e) { if (e.keyCode == 27) {hidePopup();} });
}

function hidePopup() {
    setTimeout(function(){document.querySelector('#view2').style.left = '100%';},1000);
    document.querySelector('#view2').style.opacity = '0';
}

function displayElement() {

    var label = getAllTab();
    var value = getAll();
	var overlay = {};
    for (var i = 0; i < label.length; i++) {
        overlay[label[i]] = '';
    }
	
    for(var i = 0; i < label.length;i++) {
    	var layer;

        var myStyle = {
            "color": "rgb("+Math.floor(((Math.random()*256)+200)/2)+","+Math.floor(((Math.random()*256)+220)/2)+","+Math.floor(((Math.random()*256)+180)/2)+")",
            "weight": 3,
            "opacity": 0.8,
        };
    	
    	layer = L.geoJson(value[i], {
            onEachFeature: onEachFeature,
        	style: myStyle
        }).addTo(map);
    	
    	layer.addData(value[i]);
    	
        
    	var layergroup;
    	
    	layergroup = L.layerGroup([layer]);
    	layergroup.addTo(map);

    	overlay[label[i]] =  layergroup;
    	
    }
	lcontrol = L.control.layers(null, overlay, true);
		lcontrol.addTo(map);	
}

function onEachFeature(feature, layer) {
	if(feature.properties) {
		var textPopup ="";
		if(mobile) { textPopup += "<div class='popupMobile'>"};
		for(var i in feature.properties){
			textPopup += feature.properties[i]+" ";
		}

		if(feature.properties.type == "batiment") {
			textPopup += "<br/>";
			var elem = getCampusElementsForBatiment();

			for(var i in elem){
				if(feature.properties.name == elem[i]['@attributes'].batname) {
					var nom = elem[i]['@attributes'].type;
                    if (nom != '') {
    					textPopup += "<span class='icons' style='background-image: url(../images/menubar/"+nom.replace(/ /g,'')+".png)' title='"+nom+"'></span>"  +"<span class='icons-text'>x" +elem[i]['@attributes'].unit + "</span>";
    					if(i%2 == 1) textPopup += "<br/>";
                    }
				}
				
			}
			
            if (mobile) {
                textPopup += '<br/><button class="btn-xs btn btn-primary" onclick="goBat(\''+feature.properties.name+'\')">Voir les étages</button>';
                textPopup += "</div>";
                layer.bindPopup(textPopup);
            } else {
                //console.log(layer);
                layer.on('mousemove', function(e) {
                    document.querySelector('#infoBat').innerHTML = textPopup;
                    document.querySelector('#infoBat').style.top = e.layerPoint.y + 'px';
                    document.querySelector('#infoBat').style.left = e.layerPoint.x + 'px';
					document.querySelector('#infoBat').style.display ="block";			
                });
                layer.on('mouseout', function(e) {
					document.querySelector('#infoBat').style.display = "none";
                });
                layer.on('click', function(e) {
                    goBat(feature.properties.name);
                });
            }
		}

        
	}
}

function goBat(batname) {
    window.localStorage.setItem('currentbatadmin',batname);
    document.querySelector('#view2').style.left = '0px';
    document.querySelector('#view2').style.opacity = '1';
    designView();
    displayEtageOpacity(1);
}

function displayEtageOpacity(num) {
    setTimeout(function(){
        try {
            document.querySelector('#view2 .theblock:nth-child('+num+')').style.opacity = '1';
            displayEtageOpacity(num+1);
        } catch (err) {}
    },200);
    
}
