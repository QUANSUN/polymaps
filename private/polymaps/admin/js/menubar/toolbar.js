﻿function getWidth(element) {return window.getComputedStyle(document.querySelector(element), null).getPropertyValue("width");}
function getHeight(element) {return window.getComputedStyle(document.querySelector(element), null).getPropertyValue("height");}

function buildToolbar() {
	var elements = campusXML.getElementsByTagName('element');
	var elementType = new Array();
	for (var i = 0; i < elements.length; i++) {
		var value = elements[i].attributes.type.value;
		if ( elementType.indexOf(value) < 0) {
			elementType.push(value);
		}
	}
	for (var i = 0; i < elementType.length; i++) {
		document.querySelector('#toolbar').innerHTML = '<div onclick="menuClick(\''+elementType[i]+'\')" id="'+elementType[i].replace(/ /g,'')+'">'
		+ '<img src="../images/menubar/'+ elementType[i].replace(/ /g,'') +'.png" valign="middle"/></div>'
		+ document.querySelector('#toolbar').innerHTML;
	}
	resizeToolbar();
	addToolbarListeners();
}

var selected = false;
function menuClick(type) {
	console.log(type);
	if (document.querySelector('#'+type.replace(/ /g,'')).className == 'clicked') {
		selected = false;
		document.querySelector('#'+type.replace(/ /g,'')).className = '';
	} else {
		selected = type;
		var divs = document.querySelectorAll('#toolbar > div');
		for (var i = 0; i < divs.length -1;i++) {
			divs[i].className = '';
		}
		document.querySelector('#'+type.replace(/ /g,'')).className += 'clicked';
	}

}

function resizeToolbar() {
	/** toolbar **/
	document.querySelector('#toolbar').style.height = window.innerHeight * 0.1 + 'px';

	var cells = document.querySelectorAll('#toolbar > div');
	var cellWidth = getHeight('#toolbar > div');
	for (var i = 0; i < cells.length; i++) {
		cells[i].style.width = cellWidth;
	}

	if (getHeight('#toolbar > div') != getWidth('#toolbar > div')) {
		document.querySelector('#toolbar').style.height = getWidth('#toolbar > div');
	}

	var imgs = document.querySelectorAll('#toolbar > div img');
	for (var i = 0; i < imgs.length; i++) {
		imgs[i].style.width = parseInt(cellWidth)*0.6 + 'px';
	}

	/** hide **/
	document.querySelector('#hide').style.height = window.innerHeight * 0.04 + 'px';
	document.querySelector('#hide').style.width = window.innerHeight * 0.04 + 'px';
	document.querySelector('#hide img').style.width = window.innerHeight * 0.04 + 'px';

	if (hide) {
		hideToolbar();
	}

	document.querySelector('#menubar').style.minWidth = '500px';
}

function hideToolbar() {
	document.querySelector('#toolbar').style.marginTop = '-' + document.querySelector('#toolbar').style.height;
}

function addToolbarListeners() {
	/*document.querySelector('#plus').addEventListener('click', plusAction);
	document.querySelector('#moins').addEventListener('click', moinsAction);
	document.querySelector('#save').addEventListener('click', saveAction);*/

	document.querySelector('#hide').addEventListener('click', hideAction);
}