﻿var hide = true;
function hideAction() {
	if (hide) {
		hide = false;
		document.querySelector('#menubar #hide img').style.transform = 'rotate(180deg)';
		document.querySelector('#toolbar').style.marginTop = '0px';
	} else {
		hide = true;
		document.querySelector('#menubar #hide img').style.transform = 'rotate(0deg)';
		hideToolbar();
	}
}

function saveAction() {
	getValidation();
	// envoyer le nouveau XML pour avoir une verif
}