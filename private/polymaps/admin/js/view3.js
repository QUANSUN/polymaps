﻿var map;
var lcontrol;
var layerJSON;
var layerGroup = L.layerGroup();
var layerGroupP = L.layerGroup();
var randomColor = ['blue','red','green','black','orange','yellow','brown','grey','gray'];
var timeouts = new Array();
var overlayMaps;

function initmap(campusPoint) {
    /******************************************************************************************/
    var batname = window.localStorage.getItem('currentbatadmin');

    var southWest = L.latLng(campusPoint[0][0], campusPoint[0][1]),
        northEast = L.latLng(campusPoint[1][0], campusPoint[1][1]),
        bounds = L.latLngBounds(southWest, northEast);

    map = L.map('map', {
        center: [43.61567, 7.07222],
        zoom: 18,
        minZoom: 18,
        maxZoom: 20,
        maxBounds : bounds,
		zoomControl: false,
    });

    var centre = getCentre(window.localStorage.getItem('currentbatadmin'),window.localStorage.getItem('currentetageadmin'));

    L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
    }).addTo(map);

    var zoomEff = 20;

    displayElements();
    drawSalles(batname);
    displayEscaliers(batname,window.localStorage.getItem('currentetageadmin'));
    
    setTimeout( function() {
       map.setView(centre, zoomEff);
    }, 300);
}

function displayEscaliers(batname, etagename) {
    var escaliers = search('//batiment[@nom = "'+ batname +'"]//etage[@nom = "'+etagename+'"]//escalier', campusXML);
    for (var k = 0; k < escaliers.length; k++) {
        if (parseInt(escaliers[k].attributes.etagedestination.value) > parseInt(etagename)) {
            drawEscalierT(escaliers[k].getElementsByTagName('point')[0].attributes,escaliers[k].attributes.etagedestination.value,'upescalier');
        } else {
            drawEscalierT(escaliers[k].getElementsByTagName('point')[0].attributes,escaliers[k].attributes.etagedestination.value,'downescalier');
        }
    }            
}

var escalierlayer = L.layerGroup();
function drawEscalierT(point, etagedestination, upOrDown) {
    var y = point.y.value;
    var x = point.x.value;
    var greenIcon = L.icon({
        iconUrl: './images/ionicon/'+upOrDown+'.png',
        iconSize:     [30, 30], // size of the icon
        iconAnchor:   [15, 15], // point of the icon which will correspond to marker's location
    });
    var marker = L.marker([y, x], {icon: greenIcon});
    
    marker.on('mouseover',function () {
        document.querySelector('#infoSalle').innerHTML = 'Escalier destination : '+ etagedestination;
        document.querySelector('#infoSalle').style.opacity = "1";
    });
    marker.on('mouseout',function () {
        document.querySelector('#infoSalle').style.opacity = "0";
    });
    marker.on('click', function () {
        window.localStorage.setItem('currentetageadmin',etagedestination);
        window.location.href = './View3.html';
    });
    
    escalierlayer.addLayer(marker);
}


var typeStr = new Array();
var typeLayer = new Array();
function displayElements() {
    var elementsInit = campusXML.getElementsByTagName('element');
    for (var k = 0; k < elementsInit.length; k++) {
        var index = typeStr.indexOf(elementsInit[k].attributes.type.value);
        if (index < 0) {
            typeStr.push(elementsInit[k].attributes.type.value);
            var layerInArray = L.layerGroup();
            typeLayer.push(layerInArray); 
        }
    }

    var batiments = campusXML.getElementsByTagName('batiment');
    for (var i = 0; i < batiments.length; i++) {
        if (batiments[i].attributes.nom.value == window.localStorage.getItem('currentbatadmin')) {
            var etages = batiments[i].getElementsByTagName('etage');
            for (var j = 0; j < etages.length; j++) {
                if (etages[j].attributes.nom.value == window.localStorage.getItem('currentetageadmin')) {
                    var elements = etages[j].getElementsByTagName('element');
                    for (var k = 0; k < elements.length; k++) {
                        drawElement(elements[k]);
                    }
                }
            }
        }
    }
}

function drawElement(element) {
    var type = element.attributes.type.value;
    var x = element.getElementsByTagName('point')[0].attributes.x.value;
    var y = element.getElementsByTagName('point')[0].attributes.y.value;
    var greenIcon = L.icon({
        iconUrl: '../images/menubar/'+type.replace(/ /g,'')+'.png',
        iconSize:     [40, 40], // size of the icon
        iconAnchor:   [20, 20], // point of the icon which will correspond to marker's location
    });
    var marker = L.marker([y, x], {icon: greenIcon});
    marker.on('click',function() {
        removeElement(element,marker);
    });
    var index = typeStr.indexOf(element.attributes.type.value);
    typeLayer[index].addLayer(marker);
    
}

function getCentre(batname) {
    var batiments = JSON.parse(window.localStorage.getItem('fullXML')).listbatiment.batiment;
    var resultat = new Array();
    for (var i = 0; i < batiments.length; i++) {
        if (batiments[i]['@attributes'].nom == batname) {
            resultat.push(batiments[i].centre['@attributes'].y);
            resultat.push(batiments[i].centre['@attributes'].x);
        }
    }
    return resultat;
}

function drawSalles(batname) {

    var lcontrol;
    var etagename = window.localStorage.getItem('currentetageadmin');
    var batiments = JSON.parse(window.localStorage.getItem('fullXML')).listbatiment.batiment;
    var etagePoints = new Array();
    for (var i = 0; i < batiments.length; i++) {
        if (batiments[i]['@attributes'].nom == batname) {
            var etages = batiments[i].listetage.etage;
            for (var j = 0; j < etages.length; j++) {
                if (etages[j]['@attributes'].nom == etagename) {

                    var salles = etages[j].listsalle.salle;
                    for (var k = 0; k < salles.length; k++) {
                        drawSalle(salles[k], etages[j].point);
                    }

                    var points = etages[j].point;
                    for (var k = 0; k < points.length; k++) {
                        etagePoints.push([parseFloat(points[k]['@attributes'].y),parseFloat(points[k]['@attributes'].x)]);
                    }

                    var portes = etages[j].listporte.porte;
                    for (var k = 0; k < portes.length; k++) {
                        var xt = 0;
                        var yt = 0;
                        if (portes[k].point == undefined) {
                            xt = parseFloat(portes[k][0]['@attributes'].x);
                            yt = parseFloat(portes[k][0]['@attributes'].y);
                        } else {
                            xt = parseFloat(portes[k].point['@attributes'].x);
                            yt = parseFloat(portes[k].point['@attributes'].y);
                        }

                        var circle = L.circle([yt, xt], 0.5, {
                            color: 'red',
                            fillColor: '#f03',
                            fillOpacity: 0.5
                        });

                        ajoutPorteLayout(circle);
                    }


                }
            }
            // j'ajoute le layer des portes au grouplayer
        }
    }
    var polygon = L.polygon(etagePoints,{
        color:'red',
        fillOpacity:0,
        weight:5,
        opacity:0.5
    }).addTo(map);

    polygon.on('click',onEtageClick);
    
    layerGroup.addTo(map);
    layerGroupP.addTo(map);
    overlayMaps = {
    "Salle": layerGroup,
    "Porte": layerGroupP
    };
    for (var i = 0; i < typeStr.length; i++) {
        overlayMaps[typeStr[i]] = typeLayer[i];
        typeLayer[i].addTo(map);
    }

    lcontrol = L.control.layers(null, overlayMaps, true);
    lcontrol.addTo(map);
        
}

function drawSalle(salle,etagePoints) {
    
    var points = salle.point;
    var result = new Array();
    for (var i = 0; i < points.length; i++) {
        var resultTab = new Array();
        resultTab.push(parseFloat(points[i]['@attributes'].y));
        resultTab.push(parseFloat(points[i]['@attributes'].x));
        result.push(resultTab);
    }
    
    var currentSalle = L.polygon(result);
    ajoutSalleLayout(currentSalle, salle['@attributes'].nom, salle['@attributes'].capacity);
        
 
    var portes = salle.listporte.porte;
    if (portes.length >= 0) {
        for (var i = 0; i < portes.length; i++) {
            var xt = 0;
            var yt = 0;
            if (portes[i].point == undefined) {
                xt = parseFloat(portes[i][0]['@attributes'].x);
                yt = parseFloat(portes[i][0]['@attributes'].y);
            } else {
                xt = parseFloat(portes[i].point['@attributes'].x);
                yt = parseFloat(portes[i].point['@attributes'].y);
            }
            var circle = L.circle([yt, xt], 0.35, {
                color: 'green',
                fillColor: 'green',
                fillOpacity: 0.8
            });
            ajoutPorteLayout(circle);
        }
    } else {
        var circle = L.circle([parseFloat(portes.point['@attributes'].y), parseFloat(portes.point['@attributes'].x)], 0.35, {
            color: 'green',
            fillColor: 'green',
            fillOpacity: 0.8
        });
        ajoutPorteLayout(circle);
    }
    
}

function ajoutPorteLayout(porte) {
    porte.on('click',function(e) {
        if (selected == false) {
            var lat = porte._latlng.lat;
            var lng = porte._latlng.lng;
            layerGroupP.removeLayer(porte);
            var batiments = campusXML.getElementsByTagName('batiment');
            for (var i = 0; i < batiments.length; i++) {
                if (batiments[i].attributes.nom.value == window.localStorage.getItem('currentbatadmin')) {
                    var etages = batiments[i].getElementsByTagName('etage');
                    for (var j = 0; j < etages.length; j++) {
                        if (etages[j].attributes.nom.value == window.localStorage.getItem('currentetageadmin')) {
                            var portes = etages[j].getElementsByTagName('porte');
                            for (var k = 0; k < portes.length; k++) {
                                var points = portes[k].getElementsByTagName('point');
                                for (var l = 0; l < points.length; l++) {
                                    if (points[l].attributes.x.value == lng && points[l].attributes.y.value == lat) {
                                        var x = portes[k];
                                        x.parentNode.removeChild(x);
                                        return;
                                    }
                                }
                                
                            }
                        }
                    }
                }
            }
        }
        
    });
    layerGroupP.addLayer(porte);
}

function ajoutSalleLayout(salle, nom, capacite) {
    salle.on('click',function (e) {
        onSalleClick(e,nom);
    });
    layerGroup.addLayer(salle);
}