﻿function submit() {
	$("#uploadimage").on('submit',(function(e) {
		e.preventDefault();
		$.ajax({
			url: "../php/upload.php", // Url to which the request is send
			type: "POST",             // Type of request to be send, called as method
			data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
			contentType: false,       // The content type used when sending data to the server.
			cache: false,             // To unable request pages to be cached
			processData:false,        // To send DOMDocument or non processed data file it is set to false
			success: function(data)   // A function to be called if request succeeds
			{
				console.log(data);
				if (data == "success") {
					console.log('success');
					alert('sucess');
					$('#myModal').modal('toggle');
					// on ajoute l'icon dans le menu et dans le tableau et on créé un layer
					document.querySelector('#toolbar').innerHTML = '<div onclick="menuClick(\''
					+ document.querySelector('#typeIcon').value+'\')" id="'
					+document.querySelector('#typeIcon').value.replace(/ /g,'')
					+'">'
					+ '<img src="../images/menubar/'
					+ document.querySelector('#typeIcon').value.replace(/ /g,'') 
					+'.png" valign="middle"/></div>'
					+ document.querySelector('#toolbar').innerHTML;
					resizeToolbar();
					typeStr.push(document.querySelector('#typeIcon').value.replace(/ /g,''));
					typeLayer.push(L.layerGroup());

					overlayMaps[typeStr[typeStr.length-1]] = typeLayer[typeStr.length-1];
					typeLayer[typeStr.length-1].addTo(map);
					var divs = document.querySelectorAll('.leaflet-control-container div:nth-child(2) > div');
					for (var i = 0; i < divs.length; i++) {
						divs[i].style.display = 'none';
					}

					lcontrol = L.control.layers(null, overlayMaps, true);
    				lcontrol.addTo(map);
				} else {
					console.log('error');
				}
			}
		});
	}));

	// Function to preview image after validation
	$(function() {
		$("#file").change(function() {
			var file = this.files[0];
			var imagefile = file.type;
			var match= ["image/jpeg","image/png","image/jpg"];
			if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2])))
			{
				$('#previewing').attr('src','noimage.png');
				alert("Please Select A valid Image File");
				return false;
			}
			else
			{
				var reader = new FileReader();
				reader.onload = imageIsLoaded;
				reader.readAsDataURL(this.files[0]);
			}
		});
	});
	function imageIsLoaded(e) {
		$("#file").css("color","green");
		$('#image_preview').css("display", "block");
		$('#previewing').attr('src', e.target.result);
		$('#previewing').attr('width', '250px');
		$('#previewing').attr('height', '230px');
	};
}