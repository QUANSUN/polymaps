﻿function designViewBatiment() {
	var batiments = JSON.parse(window.localStorage.getItem('fullXML')).listbatiment.batiment;

	for (var i = 0; i < batiments.length; i++) {
		document.querySelector('#etages').innerHTML += '<div onclick="goBatiment(\''+ batiments[i]['@attributes'].nom
		+'\')" class="col-sx-3 col-ms-3 col-md-3 theblock">'
		+ '<div class="etage img-thumbnail col-md-12">'
		+ '<h1>Batiment ' + batiments[i]['@attributes'].nom + '</h1>'
		+ '</div></div>';
	}
}

function goBatiment(batname) {
	window.localStorage.setItem('currentbatadmin',batname);
	document.location.href = './etages.html';
}

function designViewEtage() {
	var batiments = JSON.parse(window.localStorage.getItem('fullXML')).listbatiment.batiment;
	var batname = window.localStorage.getItem('currentbatadmin');

	for (var i = 0; i < batiments.length; i++) {
		if (batiments[i]['@attributes'].nom == batname) {
			var etages = batiments[i].listetage.etage;
			if (etages.length >= 0) {
				for (var j = 0; j < etages.length; j++) {
					designEtage(etages[j]);
				}
			} else {
				designEtage(etages);
			}
		}
	}
}

function designEtage(etage) {
	document.querySelector('#etages').innerHTML += '<div onclick="goEtage(\''+ etage['@attributes'].nom
		+'\')" class="col-sx-3 col-ms-3 col-md-3 theblock">'
		+ '<div class="etage img-thumbnail col-md-12">'
		+ '<h1>Etage ' + etage['@attributes'].nom + '</h1>'
		+ '</div></div>';
}

function goEtage(etagename) {
	window.localStorage.setItem('currentetageadmin',etagename);
	document.location.href = './View3.html';
}